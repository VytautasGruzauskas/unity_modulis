﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialObjectsControl : MonoBehaviour
{
    public Camera specialCamera;
    public GameObject[] specialObjects;
    private SpecialPower specialPower;

    // Start is called before the first frame update
    void Start()
    {
        specialPower = GameObject.FindGameObjectWithTag("Player").GetComponent<SpecialPower>();

        GameObject[] goArray = FindObjectsOfType<GameObject>();
        List<GameObject> goList = new List<GameObject>();

        for (var i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == LayerMask.NameToLayer("LevelObjectsCamClose"))
            {
                goList.Add(goArray[i]);
            }
        }

        if (goList.Count == 0)
        {
            specialObjects = null;
        }

        specialObjects = goList.ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReCheckObjects()
    {
        for (var i = 0; i < specialObjects.Length; i++)
        {
            if (specialObjects[i].GetComponent<SpecialObject>().IsObjectVisible())
            {
                SeenObject(specialObjects[i]);
            }
            else
            {
                UnSeenObject(specialObjects[i]);
            }

            if(!specialPower.IsPowerOn())
            {
                UnSeenObject(specialObjects[i]);
            }
        }
    }

    public void SeenObject(GameObject obj)
    {
        if(specialPower.IsPowerOn())
        {
            //darom kazka
            if(!obj.GetComponent<SpecialObject>().onSeenSolid)
            {
                obj.GetComponent<Collider>().enabled = false;
            }
            else
            {
                obj.GetComponent<Collider>().enabled = true;
            }

        }
    }

    public void UnSeenObject(GameObject obj)
    {
        if (!obj.GetComponent<SpecialObject>().onSeenSolid)
        {
            obj.GetComponent<Collider>().enabled = true;
        }
        else
        {
            obj.GetComponent<Collider>().enabled = false;
        }
    }
}
