﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    public float time = 0.1f;

    void Start()
    {
        Destroy(gameObject, time);
    }

}
