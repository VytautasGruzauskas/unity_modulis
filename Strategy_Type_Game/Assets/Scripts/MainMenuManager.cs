﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public Animator cameraAnimator;
    public ParticleSystem MusicLevelParticle;
    public ParticleSystem SoundLevelParticle;
    public Canvas AboutMenu;
    public AudioMixer Mixer;


    private bool isInOptionsMenu = false;
    private bool isInRankingsMenu = false;
    private bool isInAboutMenu = false;

    void Start()
    {
        cameraAnimator.SetBool("IsInOptionsMenu", isInOptionsMenu);
        cameraAnimator.SetBool("isInRankingsMenu", isInOptionsMenu);
        cameraAnimator.SetBool("isInAboutMenu", isInOptionsMenu);
        AboutMenu.gameObject.SetActive(false);

    }


    public void PressedStart()
    {
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }

    public void PressedOptions()
    {
        if(!isInOptionsMenu)
        {
            isInOptionsMenu = true;
            cameraAnimator.SetBool("IsInOptionsMenu", isInOptionsMenu);
        }
    }

    public void PressedBackOptions()
    {
        if (isInOptionsMenu)
        {
            isInOptionsMenu = false;
            cameraAnimator.SetBool("IsInOptionsMenu", isInOptionsMenu);
        }
    }

    public void MusicLevel(int level)
    {
        var main = MusicLevelParticle.main;
        main.maxParticles = level;

        if(level == 5)
            Mixer.SetFloat("musicVol", 0);
        else if (level == 4)
            Mixer.SetFloat("musicVol", -10);
        else if (level == 3)
            Mixer.SetFloat("musicVol", -20);
        else if (level == 2)
            Mixer.SetFloat("musicVol", -30);
        else if (level == 1)
            Mixer.SetFloat("musicVol", -40);
        else if (level == 0)
            Mixer.SetFloat("musicVol", -80);

        if (level == 0)
        {
            var emission = MusicLevelParticle.emission;
            emission.enabled = false;
        }
        else
        {
            var emission = MusicLevelParticle.emission;
            emission.enabled = true;
        }
    }

    public void SoundLevel(int level)
    {
        var main = SoundLevelParticle.main;
        main.maxParticles = level;

        if (level == 5)
            Mixer.SetFloat("sfxVol", 0);
        else if (level == 4)
            Mixer.SetFloat("sfxVol", -10);
        else if (level == 3)
            Mixer.SetFloat("sfxVol", -20);
        else if (level == 2)
            Mixer.SetFloat("sfxVol", -30);
        else if (level == 1)
            Mixer.SetFloat("sfxVol", -40);
        else if (level == 0)
            Mixer.SetFloat("sfxVol", -80);

        if (level == 0)
        {
            var emission = SoundLevelParticle.emission;
            emission.enabled = false;
        }
        else
        {
            var emission = SoundLevelParticle.emission;
            emission.enabled = true;
        }
    }

    public void PressedAbout()
    {
        if (!isInAboutMenu)
        {
            AboutMenu.gameObject.SetActive(true);
            isInAboutMenu = true;
            cameraAnimator.SetBool("isInAboutMenu", isInAboutMenu);
        }
    }

    public void PressedAboutBack()
    {
        if (isInAboutMenu)
        {
            AboutMenu.gameObject.SetActive(false);
            isInAboutMenu = false;
            cameraAnimator.SetBool("isInAboutMenu", isInAboutMenu);
        }
    }

    public void PressedHighRankings()
    {
        if (!isInRankingsMenu)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().RankingsCanvasActive(true);
            isInRankingsMenu = true;
            cameraAnimator.SetBool("isInRankingsMenu", isInRankingsMenu);
        }
    }

    public void PressedHighRankingsBack()
    {
        if (isInRankingsMenu)
        {
            Destroy(GameObject.Find("GameManager").GetComponent<GameManager>().RankingCanvas.gameObject);
            
            isInRankingsMenu = false;
            cameraAnimator.SetBool("isInRankingsMenu", isInRankingsMenu);
        }
    }

    public void PressedExit()
    {
        Application.Quit();
    }
}
