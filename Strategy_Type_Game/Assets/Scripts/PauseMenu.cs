﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public void PressedResume()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        manager.PauseGame(false);

        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().pauseMenuOpened = false;
        gameObject.SetActive(false);
    }

    public void PressedMainMenu()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        manager.PauseGame(false);
        manager.ClearScore();
        Cursor.lockState = CursorLockMode.Confined;
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        manager.isPlayerSpawned = false;
        SceneManager.LoadScene("Main_Menu");
    }
}
