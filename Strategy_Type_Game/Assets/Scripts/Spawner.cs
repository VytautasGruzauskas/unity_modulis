﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemyPrefab;

    public AnimationCurve SpawnIntervalInTime;
    //public float spawnInterval = 10f;
    public int maxEnemies = 1;

    private float spawnTime = 0;
    private int spawnedEnemies = 0;

    private float currentTime = 0;
    private int totalSpawned = 0;


    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
    }
    private void Update()
    {
        currentTime += Time.deltaTime;

        if ((Time.time - spawnTime) > SpawnIntervalInTime.Evaluate(currentTime))
        {
           SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        if(maxEnemies > spawnedEnemies && totalSpawned < maxEnemies)
        {
            spawnedEnemies++;
            totalSpawned++;
            if(totalSpawned >= maxEnemies)
            {
                totalSpawned = maxEnemies;
            }

            spawnTime = Time.time;
            
            GameObject enemy = Instantiate(enemyPrefab, transform.position+ new Vector3(Random.Range(0.3f, 1f), 0, Random.Range(0.3f, 1f)), Quaternion.identity);
            enemy.GetComponent<AI>().spawner = this;
        }
    }

    public void EnemyKilled()
    {
        spawnedEnemies--;

        if(spawnedEnemies <= 0)
        {
            maxEnemies *= 2;
            totalSpawned = 0;
        }
    }
}
