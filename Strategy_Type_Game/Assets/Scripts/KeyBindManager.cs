﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class KeyBindManager : MonoBehaviour
{
    public Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

    public bool isDuplicate = false;

    private void Awake()
    {
        GameObject[] managers = GameObject.FindGameObjectsWithTag("KeyBindManager");
        foreach(GameObject obj in managers)
        {
            if(obj != gameObject)
            {
                Destroy(gameObject);
            }
        }
    }

    //Loading key bindings.
    void Start()
    {
        if(!isDuplicate)
            DontDestroyOnLoad(gameObject);

        KeyBindTextManager textManager = GetComponent<KeyBindTextManager>();

        //movement
        keys.Add("Up", KeyCode.W);
        keys.Add("Down", KeyCode.S);
        keys.Add("Left", KeyCode.A);
        keys.Add("Right", KeyCode.D);
        keys.Add("Jump", KeyCode.Space);

        //Actions
        keys.Add("Shoot", KeyCode.Mouse0);
        keys.Add("Granade", KeyCode.Mouse1);
        keys.Add("Use", KeyCode.F);
        keys.Add("Special", KeyCode.Q);

        string str = PlayerPrefs.GetString("Up");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Up"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Down");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Down"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Left");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Left"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Right");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Right"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Jump");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Jump"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Shoot");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Shoot"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Granade");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Granade"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Use");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Use"] = keycodeResult;
        }

        str = PlayerPrefs.GetString("Special");
        if (str != null && str.Length > 0)
        {
            KeyCode keycodeResult = (KeyCode)Enum.Parse(typeof(KeyCode), str);
            keys["Special"] = keycodeResult;
        }

        if(textManager.enabled)
        {
            textManager.UpdateText();
        }
    }

    public void SaveKeyBindings()
    {
        PlayerPrefs.SetString("Up", keys["Up"].ToString());
        PlayerPrefs.SetString("Down", keys["Down"].ToString());
        PlayerPrefs.SetString("Left", keys["Left"].ToString());
        PlayerPrefs.SetString("Right", keys["Right"].ToString());
        PlayerPrefs.SetString("Jump", keys["Jump"].ToString());
        PlayerPrefs.SetString("Shoot", keys["Shoot"].ToString());
        PlayerPrefs.SetString("Granade", keys["Granade"].ToString());
        PlayerPrefs.SetString("Use", keys["Use"].ToString());
        PlayerPrefs.SetString("Special", keys["Special"].ToString());
        PlayerPrefs.Save();
    }

    private void OnApplicationQuit()
    {
        SaveKeyBindings();
    }
}
