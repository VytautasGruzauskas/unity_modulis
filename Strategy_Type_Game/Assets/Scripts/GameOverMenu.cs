﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI inputFieldText;
    public TextMeshProUGUI inputFieldPlaceHolderText;

    private GameManager manager;
    public bool addedScore = false;

    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManager>();
    }

    public void UpdateTexts()
    {
        scoreText.text = ""+manager.GetScore();
    }

    public void PressedMainMenu()
    {
        manager.PauseGame(false);
        Cursor.lockState = CursorLockMode.Confined;
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        manager.isPlayerSpawned = false;
        SceneManager.LoadScene("Main_Menu");
    }
    public void PressedSubmit()
    {
        if (!addedScore)
        {
            if (inputFieldText.text.Length > 15)
            {
                inputFieldText.text = "";
                inputFieldPlaceHolderText.text = "Too Long (<= 15 char)";
                return;
            }

            if (inputFieldText.text.Length < 3)
            {
                inputFieldText.text = "";
                inputFieldPlaceHolderText.text = "Too Short (> 2 char)";
                return;
            }
            manager.AddScoreToHighScore(manager.GetScore(), inputFieldText.text);
            addedScore = true;
            inputFieldText.text = "";
            inputFieldPlaceHolderText.text = "Score Added";
        }
        else
        {
            inputFieldText.text = "";
            inputFieldPlaceHolderText.text = "Score Already Added";
        }
    }

    public void PressedRestart()
    {
        manager.isPlayerSpawned = false;
        manager.ClearScore();
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }
}
