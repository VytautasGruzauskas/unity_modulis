﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialPower : MonoBehaviour
{
    public GameObject ImageMask;
    private Animator animator;
    private bool isPowerOn;
    KeyBindManager bindManager;

    public Slider PowerUpSlider;
    public float powerEnergy;
    public float maxPowerEnergy;
    private bool isOnCoolDown = false;


    // Start is called before the first frame update
    void Start()
    {
        bindManager = GameObject.FindGameObjectWithTag("KeyBindManager").GetComponent<KeyBindManager>();
        ImageMask = GameObject.FindGameObjectWithTag("SpecialPowerMask");
        PowerUpSlider = GameObject.FindGameObjectWithTag("PowerUpSlider").GetComponent<Slider>(); 

        isPowerOn = false;
        animator = ImageMask.GetComponent<Animator>();
        animator.SetBool("IsPowerOn", isPowerOn);

        powerEnergy = maxPowerEnergy;
        PowerUpSlider.maxValue = maxPowerEnergy;
        PowerUpSlider.value = powerEnergy;

        ImageMask.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(bindManager.keys["Special"]))
        {
            SwitchPower(!isPowerOn);
        }

        if(isPowerOn)
        {
            powerEnergy -= Time.deltaTime * 1.5f;
            PowerUpSlider.value = powerEnergy;


            if (powerEnergy <= 0)
            {
                SwitchPower(false);
                isOnCoolDown = true;
                StartCoroutine(WaitcoolDown(4));
                powerEnergy = 0;
            }
        }
        else if(!isPowerOn && !isOnCoolDown && powerEnergy <= maxPowerEnergy)
        {
            powerEnergy += Time.deltaTime / 0.8f;
            PowerUpSlider.value = powerEnergy;
        }
    }

    public void SwitchPower(bool turnOn)
    {
        if (!isOnCoolDown)
        {
            isPowerOn = turnOn;
            if (isPowerOn)
            {
                ImageMask.SetActive(turnOn);
                animator.SetBool("IsPowerOn", turnOn);
                Debug.Log("Power ON");
                GameObject.FindGameObjectWithTag("SpecialObjectController").GetComponent<SpecialObjectsControl>().ReCheckObjects();
            }
            else
            {
                animator.SetBool("IsPowerOn", turnOn);
                StartCoroutine(Wait());
                Debug.Log("Power OFF");
            }
        }
    }

    public bool IsPowerOn()
    {
        return isPowerOn;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.75f);
        
        ImageMask.SetActive(false);
        GameObject.FindGameObjectWithTag("SpecialObjectController").GetComponent<SpecialObjectsControl>().ReCheckObjects();
    }

    IEnumerator WaitcoolDown(float time)
    {
        yield return new WaitForSeconds(time);

        isOnCoolDown = false;
    }
}
