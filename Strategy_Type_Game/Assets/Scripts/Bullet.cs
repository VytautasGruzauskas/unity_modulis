﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Bullet : MonoBehaviour
{
    public UnityEvent OnBulletHit;
    public int Damage = 1;

    public ParticleSystem hitParticle;

    public AI.DamageType bulletDamageType = AI.DamageType.Pistol;

    private bool didHit = false;


    void Start()
    {
        Destroy(gameObject, 4f);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (!didHit)
        {
            OnBulletHit.Invoke();

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.GetComponent<PlayerController>().TakeDamage(Damage);
            }

            if (collision.gameObject.tag == "Enemy")
            {
                collision.gameObject.GetComponent<AI>().TakeDamage(Damage, bulletDamageType);
            }
            didHit = true;

            Vector3 pos = transform.position;
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, pos);
            Instantiate(hitParticle, pos, rot);


            Destroy(gameObject);
        }
    }
}
