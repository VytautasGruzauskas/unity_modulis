﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class KeyBindTextManager : MonoBehaviour
{
    KeyBindManager bindManager;

    public TextMeshPro textUp, textDown, textLeft, textRight, textJump, textShoot, textGranade, textUse, textSpecial;

    private GameObject currentKey;

    // Start is called before the first frame update
    void Start()
    {
        bindManager = GetComponent<KeyBindManager>();
        textUp = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textDown = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textLeft = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textRight = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textJump = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textShoot = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textGranade = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textUse = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textSpecial = GameObject.Find("Up").GetComponent<TextMeshPro>();
    }

    public void UpdateText()
    {
        textUp = GameObject.Find("Up").GetComponent<TextMeshPro>();
        textDown = GameObject.Find("Down").GetComponent<TextMeshPro>();
        textLeft = GameObject.Find("Left").GetComponent<TextMeshPro>();
        textRight = GameObject.Find("Right").GetComponent<TextMeshPro>();
        textJump = GameObject.Find("Jump").GetComponent<TextMeshPro>();
        textShoot = GameObject.Find("Shoot").GetComponent<TextMeshPro>();
        textGranade = GameObject.Find("Granade").GetComponent<TextMeshPro>();
        textUse = GameObject.Find("Use").GetComponent<TextMeshPro>();
        textSpecial = GameObject.Find("Special").GetComponent<TextMeshPro>();

        //setting text up
        textUp.text = "Up: " + bindManager.keys["Up"].ToString();
        textDown.text = "Down: " + bindManager.keys["Down"].ToString();
        textLeft.text = "Left: " + bindManager.keys["Left"].ToString();
        textRight.text = "Right: " + bindManager.keys["Right"].ToString();
        textJump.text = "Jump: " + bindManager.keys["Jump"].ToString();
        textShoot.text = "Shoot: " + bindManager.keys["Shoot"].ToString();
        textGranade.text = "Granade: " + bindManager.keys["Granade"].ToString();
        textUse.text = "Use: " + bindManager.keys["Use"].ToString();
        textSpecial.text = "Special: " + bindManager.keys["Special"].ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnGUI()
    {
        if (currentKey != null)
        {
            bool isOccupied = false;
            Event e = Event.current;
            if (e.isKey)
            {
                Debug.Log(" "+ e.keyCode.ToString());


                foreach (KeyValuePair<string, KeyCode> attachStat in bindManager.keys)
                {
                    if(attachStat.Value == e.keyCode)
                    {
                        isOccupied = true;
                        break;
                    }
                }

                if(!isOccupied)
                {
                    bindManager.keys[currentKey.name] = e.keyCode;
                    UpdateText();
                }

                currentKey = null;
            }
            else if(e.isMouse)
            {
                Debug.Log(" " + e.button.ToString());

                foreach (KeyValuePair<string, KeyCode> attachStat in bindManager.keys)
                {
                    switch (e.button)
                    {
                        case 0: if (attachStat.Value == KeyCode.Mouse0 ) isOccupied = true; break;
                        case 1: if (attachStat.Value == KeyCode.Mouse1 ) isOccupied = true; break;
                        case 2: if (attachStat.Value == KeyCode.Mouse2 ) isOccupied = true; break;
                        case 3: if (attachStat.Value == KeyCode.Mouse3 ) isOccupied = true; break;
                        case 4: if (attachStat.Value == KeyCode.Mouse4 ) isOccupied = true; break;
                        case 5: if (attachStat.Value == KeyCode.Mouse5 ) isOccupied = true; break;
                        case 6: if (attachStat.Value == KeyCode.Mouse6 ) isOccupied = true; break;
                    }
                    if (isOccupied) break;
                }

                if (!isOccupied)
                {
                    switch (e.button)
                    {
                        case 0: bindManager.keys[currentKey.name] = KeyCode.Mouse0; break;
                        case 1: bindManager.keys[currentKey.name] = KeyCode.Mouse1; break;
                        case 2: bindManager.keys[currentKey.name] = KeyCode.Mouse2; break;
                        case 3: bindManager.keys[currentKey.name] = KeyCode.Mouse3; break;
                        case 4: bindManager.keys[currentKey.name] = KeyCode.Mouse4; break;
                        case 5: bindManager.keys[currentKey.name] = KeyCode.Mouse5; break;
                        case 6: bindManager.keys[currentKey.name] = KeyCode.Mouse6; break;
                    }
                    UpdateText();
                }
                currentKey = null;
            }
        }
    }

    public void OnPointerClick(GameObject textObject)
    {
        Debug.Log(textObject.name + " Game Object Clicked!");
        currentKey = textObject;
    }
    
    public void OnPointerEnter(GameObject textObject)
    {
        //Debug.Log(textObject.name + " Game Object Enter!");


        TextMeshPro text = textObject.GetComponent<TextMeshPro>();

        if (text != null)
        {
            text.faceColor = new Color32(100, 100, 100, 255);
        }
    }

    public void OnPointerExit(GameObject textObject)
    {
        //Debug.Log(textObject.name + " Game Object Exit!");

        TextMeshPro text = textObject.GetComponent<TextMeshPro>();

        if (text != null)
        {
            text.faceColor = new Color32(255, 255, 255, 255);
        }
    }
}
