﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerBullet : MonoBehaviour
{
    public UnityEvent OnBulletHit;
    public int Damage = 1;
    private bool didHit = false;
    public ParticleSystem hitParticle;

    public AI.DamageType bulletDamageType = AI.DamageType.Pistol;

    void Start()
    {
        Destroy(gameObject, 4f);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (!didHit)
        {
            OnBulletHit.Invoke();


            if (collision.gameObject.tag == "Enemy")
            {
                collision.gameObject.GetComponent<AI>().TakeDamage(Damage, AI.DamageType.Pistol);
            }
            didHit = true;

            Vector3 pos = transform.position;
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, pos);
            Instantiate(hitParticle, pos, rot);
            

            Destroy(gameObject);
        }
    }
}
