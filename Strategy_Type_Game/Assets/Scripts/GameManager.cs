﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    /*
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
    */
    public Canvas RankingCanvasPrefab;
    public Canvas RankingCanvas;
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<Transform> highscoreEntryTransformList;

    private List<HighscoreEntry> highscoreEntryList;

    public GameObject PlayerPrefab;

    private Transform[] spawnPoints;

    private GameObject camera;
    private GameObject cameraSpecial;

    [SerializeField]
    private int Score = 0;

    private bool gamePaused = false;
    public bool isDuplicate = false;

    public bool isPlayerSpawned = false;


    private void Awake()
    {
        GameObject[] managers = GameObject.FindGameObjectsWithTag("GameManager");
        foreach (GameObject obj in managers)
        {
            if (obj != gameObject)
            {
                Destroy(gameObject);
            }
        }


        if(!isDuplicate)
        {
            //RankingCanvas.gameObject.SetActive(false);
        }
    }

    void Start()
    {
        if (!isDuplicate)
            DontDestroyOnLoad(gameObject);

        Cursor.lockState = CursorLockMode.Confined;
        
        if (isDuplicate)
        {
            camera = GameObject.FindGameObjectWithTag("MainCamera");
            cameraSpecial = GameObject.FindGameObjectWithTag("SpecialCamera");
            spawnPoints = GameObject.FindGameObjectWithTag("SpawnPoints").GetComponentsInChildren<Transform>();
            SpawnPlayer();
        }
    }

    // called first
    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);

        if(scene.name == "SampleScene")
        {
            camera = GameObject.FindGameObjectWithTag("MainCamera");
            cameraSpecial = GameObject.FindGameObjectWithTag("SpecialCamera");
            spawnPoints = GameObject.FindGameObjectWithTag("SpawnPoints").GetComponentsInChildren<Transform>();
            Debug.Log(spawnPoints.Length);
            SpawnPlayer();
            PauseGame(false);
        }

        if (scene.name == "Main_Menu")
        {
            //RankingCanvas = GameObject.FindGameObjectWithTag("RankingCanvas").GetComponent<Canvas>();
        }

        Debug.Log(mode);
    }


    // called when the game is terminated
    void OnDisable()
    {
        Debug.Log("OnDisable");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void SpawnPlayer()
    {
        if (!isPlayerSpawned)
        {
            isPlayerSpawned = true;
            int index = UnityEngine.Random.Range(1, spawnPoints.Length);
            GameObject player = Instantiate(PlayerPrefab, spawnPoints[index].position, spawnPoints[index].rotation);
            player.SetActive(true);
            camera.GetComponent<CameraFollow>().Player = player.transform;
            cameraSpecial.GetComponent<CameraFollow>().Player = player.transform;
        }
    }


    public void AddScore(int score)
    {
        Score += score;
    }
    
    public void ClearScore()
    {
        Score = 0;
    }

    public int GetScore()
    {
        return Score;
    }

    public void PauseGame(bool pausedState)
    {
        gamePaused = pausedState;

        if(gamePaused)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Time.timeScale = 0;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Confined;
            Time.timeScale = 1;
        }
    }

    public bool IsGamePaused()
    {
        return gamePaused;
    }

    private Highscores LoadHighScores()
    {
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores;


        if (string.IsNullOrEmpty(jsonString))
        {
            highscoreEntryList = new List<HighscoreEntry>()
            {
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
                new HighscoreEntry{ score = 0, name = "None" },
            };

            highscores = new Highscores { highscoreEntryList = highscoreEntryList };

            string json = JsonUtility.ToJson(highscores);
            PlayerPrefs.SetString("highscoreTable", json);
            PlayerPrefs.Save();
        }
        else
        {
            highscores = JsonUtility.FromJson<Highscores>(jsonString);
        } 

        //sort list
        for(int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for(int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                if(highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        return highscores;
    }

    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 35f;

        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        string rankString = "";
        switch (rank)
        {
            default: rankString = rank + "TH"; break;

            case 1: rankString = "1ST"; break;
            case 2: rankString = "2ND"; break;
            case 3: rankString = "3RD"; break;
        }

        entryTransform.Find("posText").GetComponent<TextMeshProUGUI>().text = rankString;

        int score = highscoreEntry.score;
        entryTransform.Find("scoreText").GetComponent<TextMeshProUGUI>().text = score.ToString();

        string name = highscoreEntry.name;
        entryTransform.Find("nameText").GetComponent<TextMeshProUGUI>().text = name;

        transformList.Add(entryTransform);
    }

    public void RankingsCanvasActive(bool active)
    {
        RankingCanvas = Instantiate(RankingCanvasPrefab, Vector3.zero, Quaternion.identity);
        entryContainer = GameObject.Find("highscoreEntryContainer").GetComponent<Transform>();
        entryTemplate = GameObject.Find("HighScoreEntryTemplate").GetComponent<Transform>();
        entryTemplate.gameObject.SetActive(false);


        if (active)
        {
            Highscores highscores = LoadHighScores();

            highscoreEntryTransformList = new List<Transform>();
            int num = 0;
            foreach (HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
            {
                if (num >= 10) break;
                CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
                num++;
            }
        }
    }

    public void AddScoreToHighScore(int Score, string name)
    {
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = Score, name = name };

        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        highscores.highscoreEntryList.Add(highscoreEntry);


        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    [System.Serializable]
    private class HighscoreEntry
    {
        public int score;
        public string name;
    }
}


