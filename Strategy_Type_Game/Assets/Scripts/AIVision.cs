﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIVision : MonoBehaviour
{
    public bool IsGranadeCheck = false;
    public bool isPlayerInVision = false;
    public bool isObsticleInVision = false;

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player" && other.tag != "Enemy" && other.tag != "VisionFieldGranade")
        {
            isPlayerInVision = true;
        }

        if(IsGranadeCheck && other.tag != "Enemy" && other.tag != "VisionField")
        {
            isObsticleInVision = true;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && other.tag != "Enemy" && other.tag != "VisionFieldGranade")
        {
            isPlayerInVision = true;
        }
        if (IsGranadeCheck && other.tag != "Enemy" && other.tag != "VisionField")
        {
            isObsticleInVision = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && other.tag != "Enemy" && other.tag != "VisionFieldGranade")
        {
            isPlayerInVision = false;
        }
        if (IsGranadeCheck && other.tag != "Enemy" && other.tag != "VisionField")
        {
            isObsticleInVision = false;
        }
    }
}
