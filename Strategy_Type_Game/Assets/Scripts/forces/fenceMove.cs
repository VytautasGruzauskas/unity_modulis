﻿using UnityEngine;

public class fenceMove : MonoBehaviour
{
    public GameObject Player;
    public Rigidbody Fence;
    public float power = 2f;

    private void OnTriggerStay(Collider other)
    {
        if(other.name == Player.name)
        {
            Fence.AddRelativeForce((new Vector3(1,0,0) * power), ForceMode.Force);
            Debug.Log("adding "+power+" force");
        }
    }
}
