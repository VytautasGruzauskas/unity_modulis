﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxPush : MonoBehaviour
{
    public float power;
    public Vector3 forceDir = new Vector3(1, 0, 0);
    public bool ForwardDirection = false;
    private void OnTriggerStay(Collider other)
    {
        
        Rigidbody rb;
        if(rb = other.gameObject.GetComponent<Rigidbody>())
        {
            if(ForwardDirection)
                rb.AddForce(gameObject.transform.forward * power, ForceMode.Impulse);
            else
                rb.AddForce(forceDir * power, ForceMode.Impulse);
        }
    }
}
