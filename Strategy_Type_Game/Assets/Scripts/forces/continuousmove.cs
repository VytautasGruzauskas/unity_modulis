﻿using UnityEngine;

public class continuousmove : MonoBehaviour
{
    public Rigidbody Object;
    public Vector3 forceVector;
    public float power = 1f;
    public bool isRandom = false;


    void Update()
    {
        if(isRandom)
        {
            float[] pos = { 0,0,0 };

            for (int i = 0; i<3; i++)
            {
                pos[i] = Random.Range(-2.1f, i+1);
            }

            power = Random.Range(-1.2f, 1.2f);

            forceVector = new Vector3(pos[0]+2, pos[1]+1, pos[2]);
        }
        //Debug.Log(forceVector);
        Object.AddRelativeForce((forceVector * power), ForceMode.Impulse);
    }
}
