﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenFollow : MonoBehaviour
{
    public Transform target;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        //follow the target object
        transform.Translate(target.transform.position - transform.position);
    }
}
