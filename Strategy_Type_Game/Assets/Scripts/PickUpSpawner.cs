﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawner : MonoBehaviour
{
    public GameObject HealthPickUpPrefab;
    public GameObject AmmoPickUpPrefab;
    public GameObject AmmoShotgunPickUpPrefab;
    public GameObject GranadePickUpPrefab;


    public Transform[] spawnLocations;
    private bool[] spawnLocationsOccupied;

    public float spawnInterval = 30f;

    public bool LimitedPickUps = true;
    public int maxPickUps = 10;

    private float spawnTime = 0;
    private int spawnedPickUps = 0;

    private void Start()
    {
        spawnLocationsOccupied = new bool[spawnLocations.Length];
        for(int i = 0; i < spawnLocationsOccupied.Length; i++)
        {
            spawnLocationsOccupied[i] = false;
        }
    }

    private void Update()
    {
        if ((Time.time - spawnTime) > spawnInterval)
        {
            SpawnPickUp();
        }
    }

    void SpawnPickUp()
    {
        GameObject PickUp;
        int rand = Random.Range(0, spawnLocations.Length);

        int last = -1, spawnPointsNum = -1;

        if(spawnLocationsOccupied[rand])
        {
            spawnPointsNum = 0;
            for (int i = 0; i < spawnLocations.Length; i++)
            {
                if(!spawnLocationsOccupied[i])
                {
                    last = i;
                    spawnPointsNum++;
                }
            }
        }

        if(spawnPointsNum == 0)
        {
            return;
        }
        else if (spawnPointsNum > 0)
        {
            rand = last;
        }


        if (maxPickUps > spawnedPickUps && LimitedPickUps)
        {
            
            int rand2 = Random.Range(0, 4);
            spawnedPickUps++;
            spawnTime = Time.time;
            if(rand2 == 0)
                PickUp = Instantiate(HealthPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else if(rand2 == 1)
                PickUp = Instantiate(AmmoPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else if(rand2 == 2)
                PickUp = Instantiate(AmmoShotgunPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else
                PickUp = Instantiate(GranadePickUpPrefab, spawnLocations[rand].position, Quaternion.identity);

            PickUp.GetComponent<PickUp>().spawner = this;
            PickUp.GetComponent<PickUp>().spawnNumber = rand;
            spawnLocationsOccupied[rand] = true;
        }
        else if(!LimitedPickUps)
        {
            int rand2 = Random.Range(0, 4);
            spawnTime = Time.time;
            if (rand2 == 0)
                PickUp = Instantiate(HealthPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else if (rand2 == 1)
                PickUp = Instantiate(AmmoPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else if (rand2 == 2)
                PickUp = Instantiate(AmmoShotgunPickUpPrefab, spawnLocations[rand].position, Quaternion.identity);
            else
                PickUp = Instantiate(GranadePickUpPrefab, spawnLocations[rand].position, Quaternion.identity);

            PickUp.GetComponent<PickUp>().spawner = this;
            PickUp.GetComponent<PickUp>().spawnNumber = rand;
            spawnLocationsOccupied[rand] = true;
        }
    }


    public void PickUpPickedUp(int number)
    {
        spawnLocationsOccupied[number] = false;
    }
}
