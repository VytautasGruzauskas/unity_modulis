﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartingTextChanger : MonoBehaviour
{
    KeyBindManager bindManager;

    TextMeshPro text;

    void Start()
    {
        text = GetComponent<TextMeshPro>();
        bindManager = GameObject.FindGameObjectWithTag("KeyBindManager").GetComponent<KeyBindManager>();

        text.text = bindManager.keys["Up"] + " " + bindManager.keys["Down"] + " " + bindManager.keys["Left"] + " " + bindManager.keys["Right"] + " :WALK\n" +
            bindManager.keys["Jump"] + " :JUMP\n" +
            bindManager.keys["Shoot"] + " :SHOOT\n" +
            bindManager.keys["Granade"] + " :GRANADE\n" +
            bindManager.keys["Use"] + " :CHANGE WEAPON\n" +
            bindManager.keys["Special"] + " :SPECIAL";
    }
}
