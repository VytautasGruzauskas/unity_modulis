﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Granade : MonoBehaviour
{
    public int damage = 50;
    public GameObject ExplosionEffectPrefab;
    public float radius = 5.0f;
    public float power = 10.0f;
    public GameObject AudioPrefab;

    private bool AIthrown = false;

    private void Start()
    {

    }

    public void AIThrown()
    {
        AIthrown = true;
    }

    void Explode()
    {
        //Instantiate audio prefab to separate granade sounds
        Instantiate(AudioPrefab, gameObject.transform.position, gameObject.transform.rotation);
        //Instantiate explosion particle system
        Instantiate(ExplosionEffectPrefab, gameObject.transform.position, gameObject.transform.rotation);

        //calculate if the enemy or the player has been hit by the explosion range.
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            AI enemy = hit.GetComponent<AI>();
            PlayerController player = hit.GetComponent<PlayerController>();

            if (enemy != null)
            {
                enemy.TakeDamage(damage, AI.DamageType.Granade, AIthrown);
            }

            if (player != null)
            {
                player.TakeDamage(damage);
            }

            //if it is a rigidbody then apply some force to push it away.
            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);

        }
        //destroy the granade object.
        Destroy(gameObject, 0.1f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag != "Player")
        {
            Explode();
            //Debug.Log("EXPLODE GRANADA");
        }
    }
}
