﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialObject : MonoBehaviour
{
    public bool onSeenSolid = false;
    private SpecialObjectsControl controller;

    private bool isVisible = false;

    void Start()
    {
        controller = GameObject.FindGameObjectWithTag("SpecialObjectController").GetComponent<SpecialObjectsControl>();
    }

    public bool IsObjectVisible()
    {
        return isVisible;
    }

    void OnBecameVisible()
    {
        controller.SeenObject(gameObject);
        //Debug.Log("Visable");
        isVisible = true;
    }

    void OnBecameInvisible()
    {
        controller.UnSeenObject(gameObject);
        //Debug.Log("Invisable");
        isVisible = false;
    }
}
