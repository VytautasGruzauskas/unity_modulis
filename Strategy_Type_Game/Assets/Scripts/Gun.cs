﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class Gun : MonoBehaviour
{
    public bool isPlayer = false;
    public enum GunType {Pistol = 0, ShotGun = 1};

    public GunType gunType = GunType.Pistol;

    public int shotGunFiredRounds = 3;

    public Image ammoImage;
    public Sprite[] ammoSprite;

    public GameObject[] bulletPrefabs;
    public float bulletPower = 20f;
    public Transform[] GunPoint;


    public GameObject MuzzleFlashPrefab;
    public Text AmmoText;
    float startingPitch = 0;

    public int[] maxMagazine;
    public int[] maxAmmo;
    [SerializeField]
    private int[] ammo;
    private int[] magazine;

    public GameObject soundPrefab;
    public AudioClip shootSound;
    public AudioClip reloadSound;
    public AudioClip noAmmoSound;
    AudioSource audioSource;
    public UnityEvent OnPlayerShoot;


    private int firedRounds = 0;
    private int bulletPoint = 0;

    void Start()
    {
        ammoImage = GameObject.FindGameObjectWithTag("AmmoImage").GetComponent<Image>();
        AmmoText = GameObject.FindGameObjectWithTag("AmmoText").GetComponent<Text>();

        //Debug.Log("Eina " + (int)GunType.Pistol + "   " + (int)GunType.ShotGun);
        ammo = new int[System.Enum.GetValues(typeof(GunType)).Length];
        magazine = new int[System.Enum.GetValues(typeof(GunType)).Length];

        if(isPlayer)
            ammoImage.sprite = ammoSprite[(int)gunType];

        ammo[(int)GunType.Pistol] = maxAmmo[(int)GunType.Pistol];
        ammo[(int)GunType.ShotGun] = maxAmmo[(int)GunType.ShotGun];
        magazine[(int)GunType.Pistol] = maxMagazine[(int)GunType.Pistol];
        magazine[(int)GunType.ShotGun] = maxMagazine[(int)GunType.ShotGun];
        audioSource = GetComponent<AudioSource>();
        startingPitch = audioSource.pitch;
        UpdateAmmoText();
    }

    public void ChangeGun(GunType gunType)
    {
        this.gunType = gunType;
        if (isPlayer)
        {
            ammoImage.sprite = ammoSprite[(int)gunType];
            UpdateAmmoText();
        }
    }

    public void Shoot()
    {
        int index = (int)gunType;


        switch (gunType)
        {
            default:
                break;


            case GunType.Pistol:
                {
                    if (magazine[index] > 0)
                    {
                        GameObject bullet = Instantiate(bulletPrefabs[index], GunPoint[0].position, GunPoint[0].rotation);//creating the bullet

                        if (isPlayer)
                            bullet.GetComponent<PlayerBullet>().bulletDamageType = AI.DamageType.Pistol;
                        else
                            bullet.GetComponent<Bullet>().bulletDamageType = AI.DamageType.Pistol;


                        bullet.GetComponent<Rigidbody>().AddForce(GunPoint[0].forward * bulletPower, ForceMode.Impulse);//adding forces to the bullet in the correct direction
                        //Debug.Log("-- is "+index);
                        magazine[index]--;
                        PlayGunSound();//playing the sound


                        Instantiate(MuzzleFlashPrefab, GunPoint[0].position + (GunPoint[0].forward * 1.2f), GunPoint[0].rotation);//creating the muzzle flash
                        OnPlayerShoot.Invoke();//invoking an event so the camera script can initiate the camera shake
                        UpdateAmmoText();
                    }
                    else
                    {
                        StartCoroutine(Reload(gunType));
                    }

                    break;
                }

            case GunType.ShotGun:
                {
                    if (magazine[index] > 0)
                    {
                        firedRounds = 1;
                        bulletPoint = 0;
                        StartCoroutine(ShootMachineGun());


                        Instantiate(MuzzleFlashPrefab, GunPoint[0].position + (GunPoint[0].forward * 1.2f), GunPoint[0].rotation);//creating the muzzle flash
                        OnPlayerShoot.Invoke();//invoking an event so the camera script can initiate the camera shake
                        UpdateAmmoText();
                    }
                    else
                    {
                        StartCoroutine(Reload(gunType));
                    }


                    break;
                }
        }
    }

    IEnumerator ShootMachineGun()
    {
        int index = (int)GunType.ShotGun;
        

        if (magazine[index] <= 0)
        {
            yield return null;

        }
        else
        {
            yield return new WaitForSeconds(0.01f);
            //Debug.Log("bulletPoint: " + bulletPoint);
            magazine[index]--;
            PlayGunSound();//playing the sound
            //Debug.LogWarning("index "+index);
            GameObject bullet = Instantiate(bulletPrefabs[index], GunPoint[bulletPoint].position, GunPoint[bulletPoint].rotation);//creating the bullet
            
            if (isPlayer)
            {
                bullet.GetComponent<PlayerBullet>().bulletDamageType = AI.DamageType.ShotGun;
                bullet.GetComponent<PlayerBullet>().Damage = 2;
                //bullet.GetComponent<Material>().color = Color.red;
            }
            else
            {
                bullet.GetComponent<Bullet>().bulletDamageType = AI.DamageType.ShotGun;
                bullet.GetComponent<Bullet>().Damage = 2;
                //bullet.GetComponent<Material>().color = Color.red;
            }

            bullet.GetComponent<Rigidbody>().AddForce(GunPoint[bulletPoint].forward * bulletPower, ForceMode.Impulse);//adding forces to the bullet in the correct direction
            if (firedRounds < shotGunFiredRounds)
            {
                firedRounds++;
                bulletPoint++;

                if (bulletPoint >= GunPoint.Length) bulletPoint = 0;

                StartCoroutine(ShootMachineGun());
            }
            UpdateAmmoText();
        }
    }

    IEnumerator Reload(GunType type)
    {
        int index = (int)type;
        if (ammo[index] > 0)
        {
            PlayReloadSound();

            yield return new WaitForSeconds(1f);

            //Debug.Log("Eina"+magazine[index]+"   "+maxMagazine[index]);
            if (magazine[index] < maxMagazine[index])
            {
                //Debug.Log("Praeina");
                int neededAmmo = maxMagazine[index] - magazine[index];
                
                if(ammo[index] >= neededAmmo)
                {
                    magazine[index] = maxMagazine[index];
                    ammo[index] -= neededAmmo;
                }
                else
                {
                    magazine[index] += ammo[index];
                    ammo[index] = 0;
                }

                UpdateAmmoText();
            }
        }
        else
        {
            PlayNoAmmoSound();
            //Debug.Log("NO AMMO");
            yield return null;
        }

        yield return null;
    }

    void PlayGunSound()
    {
        switch(gunType)
        {
            case GunType.Pistol:
                {
                    GameObject sound = Instantiate(soundPrefab, gameObject.transform.position, gameObject.transform.rotation);
                    sound.GetComponent<AudioSource>().clip = shootSound;//selecting the clip to play
                    float rand = Random.Range(0.9f, 1.1f);//chosing random number from 0.9 to 1.1
                    sound.GetComponent<AudioSource>().pitch = rand;//changing the sound pitch
                    sound.GetComponent<AudioSource>().Play();//playing the sound
                    break;
                }

            case GunType.ShotGun:
                {
                    GameObject sound = Instantiate(soundPrefab, gameObject.transform.position, gameObject.transform.rotation);
                    sound.GetComponent<AudioSource>().clip = shootSound;
                    float rand = Random.Range(0.3f, 0.5f);//chosing random number from 0.3 to 0.5
                    sound.GetComponent<AudioSource>().pitch = rand;//changing the sound pitch
                    sound.GetComponent<AudioSource>().Play();//playing the sound
                    break;
                }
        }
    }

    void PlayReloadSound()
    {
        audioSource.pitch = 1;
        audioSource.clip = reloadSound;
        audioSource.Play();//playing the sound
    }

    void PlayNoAmmoSound()
    {
        audioSource.pitch = 1;
        audioSource.clip = noAmmoSound;
        audioSource.Play();//playing the sound
    }


    void UpdateAmmoText()
    {
        if (isPlayer)
        {
            ammoImage.sprite = ammoSprite[(int)gunType];
            AmmoText.text = magazine[(int)gunType] + "/" + ammo[(int)gunType];
        }
    }

    public void SetAmmo(GunType gunType, int ammo, bool addAmmo = false)
    {
        int index = (int)gunType;
        if (addAmmo)
        {
            this.ammo[index] += ammo;
            if (ammo > maxAmmo[index]) ammo = maxAmmo[index];
        }
        else
        {
            this.ammo[index] = ammo;
        }

        UpdateAmmoText();
    }

    public void SetAmmo(GunType gunType, int ammo, int magazine)
    {
        int index = (int)gunType;
        this.ammo[index] = ammo;
        this.magazine[index] = magazine;
        UpdateAmmoText();
    }

    public void SetAmmo(GunType gunType, int ammo, int maxAmmo, int maxMagazine)
    {
        int index = (int)gunType;
        magazine[index] = maxMagazine;
        this.ammo[index] = ammo;
        this.maxAmmo[index] = maxAmmo;
        this.maxMagazine[index] = maxMagazine;
        UpdateAmmoText();
    }
}
