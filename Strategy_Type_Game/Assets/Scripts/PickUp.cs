﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public enum PickUpType { AmmoPistol, AmmoShotGun, Health, Granade };
    public PickUpType pickUpType;
    public int addedHealth = 10;
    public int addedAmmo = 20;
    public int addedAmmoShotgun = 12;
    public int addedGranades = 2;

    public int spawnNumber;
    public PickUpSpawner spawner;
    private PlayerController contr;

    void Start()
    {
        
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            contr = collision.gameObject.GetComponent<PlayerController>();
            switch (pickUpType)
            {
                case PickUpType.AmmoPistol:
                    {
                        contr.gun.SetAmmo(Gun.GunType.Pistol, addedAmmo, true);
                        spawner.PickUpPickedUp(spawnNumber);
                        Destroy(gameObject);

                         break;
                    }

                case PickUpType.AmmoShotGun:
                    {
                        contr.gun.SetAmmo(Gun.GunType.ShotGun, addedAmmoShotgun, true);
                        spawner.PickUpPickedUp(spawnNumber);
                        Destroy(gameObject);

                        break;
                    }

                case PickUpType.Granade:
                    {
                        contr.AddGranades(addedGranades);
                        spawner.PickUpPickedUp(spawnNumber);
                        Destroy(gameObject);
                        break;
                    }

                case PickUpType.Health:
                    {
                        if (contr.Health < 100)
                        {
                            if (contr.Health > contr.MaxHealth)
                            {
                                return;
                            }
                            else
                            {
                                contr.Health += addedHealth;

                                if (contr.Health > contr.MaxHealth)
                                {
                                    contr.Health = contr.MaxHealth;
                                }
                                spawner.PickUpPickedUp(spawnNumber);
                                Destroy(gameObject);
                            }
                            contr.UpdateHealthSlider();
                            Debug.LogWarning("Pasigyde");
                        }
                        break;
                    }
            }
            
        }
    } 
}
