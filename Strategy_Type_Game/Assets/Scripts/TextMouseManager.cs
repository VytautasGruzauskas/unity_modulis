﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMouseManager : MonoBehaviour
{
    public bool isKeyBinder = false;

    public enum PressedUITarget
    {
        Nothing = 0,
        Exit = 1,
        Options = 2,
        Start = 3,
        BackOptions = 4,
        MusicLevel1 = 5,
        MusicLevel2 = 6,
        MusicLevel3 = 7,
        MusicLevel4 = 8,
        MusicLevel5 = 9,
        SoundLevel1 = 10,
        SoundLevel2 = 11,
        SoundLevel3 = 12,
        SoundLevel4 = 13,
        SoundLevel5 = 14,
        SoundLevelOff = 15,
        MusicLevelOff = 16,
        About = 17,
        HighRankings = 18,
        AboutBack = 19,
        HighRankingBack = 20
    };
    public PressedUITarget target;

    KeyBindTextManager bindManager;
    MainMenuManager mainMenuManager;

    void Start()
    {
        
        bindManager = GameObject.FindGameObjectWithTag("KeyBindManager").GetComponent<KeyBindTextManager>();
        mainMenuManager = GameObject.FindGameObjectWithTag("MainMenuManager").GetComponent<MainMenuManager>();
    }


    void OnMouseOver()
    {
        bindManager.OnPointerEnter(gameObject);
        //If your mouse hovers over the GameObject with the script attached, output this message
        //Debug.Log("Mouse is over GameObject.");
    }

    void OnMouseExit()
    {
        bindManager.OnPointerExit(gameObject);
        //The mouse is no longer hovering over the GameObject so output this message each frame
        //Debug.Log("Mouse is no longer on GameObject.");
    }

    private void OnMouseUpAsButton()
    {
        if(isKeyBinder)
        {
            bindManager.OnPointerClick(gameObject);
        }
        else
        {
            switch (target)
            {
                case PressedUITarget.Nothing:// nothing
                    {break;}

                case PressedUITarget.Exit://exit
                    {
                        mainMenuManager.PressedExit();
                        break;
                    }

                case PressedUITarget.Options://options
                    {
                        mainMenuManager.PressedOptions();
                        break;
                    }

                case PressedUITarget.Start://start
                    {
                        mainMenuManager.PressedStart();
                        break;
                    }
                case PressedUITarget.BackOptions://back from options
                    {
                        mainMenuManager.PressedBackOptions();
                        break;
                    }

                case PressedUITarget.About:
                    { mainMenuManager.PressedAbout(); break; }

                case PressedUITarget.HighRankings:
                    { mainMenuManager.PressedHighRankings(); break; }

                case PressedUITarget.AboutBack:
                    { mainMenuManager.PressedAboutBack(); break; }

                case PressedUITarget.HighRankingBack:
                    { mainMenuManager.PressedHighRankingsBack(); break; }

                case PressedUITarget.MusicLevel1:
                    {mainMenuManager.MusicLevel(1);break;}
                case PressedUITarget.MusicLevel2:
                    { mainMenuManager.MusicLevel(2); break; }
                case PressedUITarget.MusicLevel3:
                    { mainMenuManager.MusicLevel(3); break; }
                case PressedUITarget.MusicLevel4:
                    { mainMenuManager.MusicLevel(4); break; }
                case PressedUITarget.MusicLevel5:
                    { mainMenuManager.MusicLevel(5); break; }
                case PressedUITarget.MusicLevelOff:
                    { mainMenuManager.MusicLevel(0); break; }

                case PressedUITarget.SoundLevel1:
                    { mainMenuManager.SoundLevel(1); break; }
                case PressedUITarget.SoundLevel2:
                    { mainMenuManager.SoundLevel(2); break; }
                case PressedUITarget.SoundLevel3:
                    { mainMenuManager.SoundLevel(3); break; }
                case PressedUITarget.SoundLevel4:
                    { mainMenuManager.SoundLevel(4); break; }
                case PressedUITarget.SoundLevel5:
                    { mainMenuManager.SoundLevel(5); break; }
                case PressedUITarget.SoundLevelOff:
                    { mainMenuManager.SoundLevel(0); break; }
            }

        }
    }
}
