﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AI : MonoBehaviour
{
    public int MaxHealth = 50;
    [SerializeField]
    private float Health;

    public float jumpHeight = 8f;
    bool didJump = false;
    public Rigidbody rb;
    bool isGrounded = false;
    float distToGround;

    private Transform target;
    public Gun gun;

    public float maximumLookDistance = 20;
    public float maximumAttackDistance = 10;
    public float minimumDistanceFromPlayer = 1;

    public float rotationDamping = 1;

    public float shotInterval = 0.5f;
    private float shotTime = 0;


    public enum DamageType { Granade, Pistol, ShotGun };

    public Spawner spawner;
    NavMeshAgent agent;

    public Image healthBar;
    public Canvas canvas;
    private Transform targetCamera;

    public AIVision vision;
    public AIVision visionGranade;


    private bool waitForGranade = false;
    public Transform GranadePoint;
    public float GranadeThrowPower = 10f;
    public GameObject granadePrefab;

    private float AIMiddlePointHeight = 2.1f;


    public int[] ScoreOfDamage = new int[System.Enum.GetValues(typeof(DamageType)).Length];

    private bool isDead = false;

    private void Start()
    {
        targetCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = minimumDistanceFromPlayer;
        agent.isStopped = true;
        Health = MaxHealth;
        distToGround = GetComponent<Collider>().bounds.extents.y;

        int index = Random.Range(0, 2);

        if(index == 0)
            gun.ChangeGun(Gun.GunType.Pistol);
        else
            gun.ChangeGun(Gun.GunType.ShotGun);
    }

    void Update()
    {
        if (Health <= 0)
        {
            isDead = true;
            OnDeath();
        }
        else if(isGrounded && !agent.enabled && !didJump)
        {
            rb.isKinematic = true;
            agent.enabled = true;
        }
        if (!isDead && !GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().isPlayerDead())
        {
            CheckGround();
            RotateInfoCanvas();
            var distance = Vector3.Distance(target.position, transform.position);

            if (distance <= maximumLookDistance)
            {
                LookAtTarget();
                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, maximumAttackDistance))
                {
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    // Debug.Log("Did Hit");

                    
                        //Debug.Log("ausktis: "+ (gameObject.transform.position.y - target.position.y - AIMiddlePointHeight));


                    if ((gameObject.transform.position - target.position).magnitude < 6 && visionGranade.isPlayerInVision &&
                        Mathf.Abs(gameObject.transform.position.y - AIMiddlePointHeight - target.position.y + 1.36f)  > 0.15f)
                    {
                        //Debug.Log("dabar gal sokam ?");
                        if (!didJump)
                        {
                            agent.enabled = false;
                            rb.isKinematic = false;
                            //Debug.LogWarning("jumpHeight: "+jumpHeight + " math: "+Mathf.Sqrt(2 * jumpHeight ));
                            rb.AddForce(transform.up * Mathf.Sqrt(2 * jumpHeight), ForceMode.VelocityChange);
                            didJump = true;
                            StartCoroutine(JumpCoolDown());
                        }

                    }

                    if (hit.collider.gameObject.tag == "Player")
                    {
                        

                        waitForGranade = false;
                        if (hit.collider.gameObject.GetComponent<PlayerController>().isPlayerDead())
                        {
                            target = null;
                        }

                        //Check distance and time
                        if (distance <= maximumAttackDistance && (Time.time - shotTime) > shotInterval)
                        {
                            if(didJump && agent.enabled)
                                agent.isStopped = true;

                            Shoot();
                           // Debug.Log("Saudo");
                        }
                    }
                    else
                    {
                        if (agent.pathStatus == NavMeshPathStatus.PathPartial && vision.isPlayerInVision && !waitForGranade)
                        {
                            waitForGranade = true;
                            StartCoroutine(WaitForGranade());
                            LookAtTarget();
                            if (visionGranade.isObsticleInVision && distance < maximumAttackDistance)
                            {
                                agent.isStopped = true;
                                WalkToTarget();
                                //Debug.LogWarning("Atsitraukiam!!");
                            }
                        }

                        if (agent.enabled)
                        {
                            agent.isStopped = true;
                            WalkToTarget();
                        }
                        //Debug.Log("EINAM");
                    }
                }
                else if (vision.isPlayerInVision && Mathf.Abs(gameObject.transform.position.y - AIMiddlePointHeight - target.position.y + 1.36f) > 0.15f)
                {
                    //Debug.Log("EINAM nematant zemiau ar auksciau esant zaidejui");
                    if (agent.enabled)
                    {
                        agent.isStopped = true;
                        WalkToTarget();
                    }
                }
            }
        }
    }

    IEnumerator WaitForGranade()
    {
        LookAtTarget();
        yield return new WaitForSeconds(2);
        LookAtTarget();
        //new WaitForSeconds(0.5f);
        if (vision.isPlayerInVision && waitForGranade)
        {
            //Debug.LogWarning("Granade Thrown!!");
            GranadeThrow();
        }
        else
        {
            if (agent.enabled)
            {
                WalkToTarget();
                waitForGranade = false;
                agent.stoppingDistance = minimumDistanceFromPlayer;
            }
        }
    }

    void GranadeThrow()
    {
        GameObject granade = Instantiate(granadePrefab, GranadePoint.position, GranadePoint.rotation);
        granade.GetComponent<Granade>().AIThrown();
        Rigidbody rb = granade.GetComponent<Rigidbody>();

        Vector3 dir = GranadePoint.forward + Vector3.up;
        rb.AddForce(dir * GranadeThrowPower, ForceMode.Impulse);
    }

    void LookAtTarget()
    {
        if (target != null)
        {
            var dir = target.position - transform.position;
            dir.y = 0;
            var rotation = Quaternion.LookRotation(dir);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
        }
    }


    void Shoot()
    {
        //Reset the time when we shoot
        shotTime = Time.time;
        gun.Shoot();
    }

    void WalkToTarget()
    {
        if (target != null)
        {
            if (agent.isStopped)
            {
                agent.isStopped = false;

                if(waitForGranade && visionGranade.isObsticleInVision)
                {
                    var dir = target.position - transform.position;
                    dir.y = 0;
                    dir = -dir;
                    Vector3 pos = transform.position + dir/3;
                    agent.SetDestination(pos);

                }
                else
                    agent.SetDestination(target.position);
            }
        }
    }

    public void TakeDamage(int Damage, DamageType damageType, bool AIGranade = false)
    {
        GameManager manager = FindObjectOfType<GameManager>();

        switch(damageType)
        {
            default: break;

            case DamageType.Granade:
                {
                    if (!AIGranade)
                    {
                        manager.AddScore(50);
                    }
                    break;
                }

            case DamageType.Pistol:
                {
                    manager.AddScore(10);
                    break;
                }

            case DamageType.ShotGun:
                {
                    manager.AddScore(25);
                    break;
                }
        }
        healthBar.fillAmount = Health / MaxHealth;
        Health -= Damage;
        //Debug.Log(gameObject.name + " has taken " + Damage + " Damage.  float: "+(Health / MaxHealth));
    }

    void OnDeath()
    {
        if(spawner != null)
            spawner.EnemyKilled();
        Destroy(gameObject);
    }

    void RotateInfoCanvas()
    {
        canvas.transform.LookAt(targetCamera);
    }

    void CheckGround()
    {
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    IEnumerator JumpCoolDown()
    {
        yield return new WaitForSeconds(1f);
        
        didJump = false;
    }
}
