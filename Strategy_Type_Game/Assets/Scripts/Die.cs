﻿using UnityEngine;

public class Die : MonoBehaviour
{
    public float Time = 3f;
    void Start()
    {
        Destroy(gameObject, Time);
    }
}
