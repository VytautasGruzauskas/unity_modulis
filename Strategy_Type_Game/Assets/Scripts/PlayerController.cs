﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Camera mainCamera;
    public int MaxHealth = 100;
    public int Health = 100;
    public float speed = 1f;
    public float jumpHeight = 8f;
    public float gravity = 9.8f;
    Rigidbody rb;
    public Gun gun;
    public int maxGranades;
    [SerializeField]
    private int granades;
    public Text granadeText;
    public GameObject granadePrefab;
    public Transform GranadePoint;
    public float GranadeThrowPower = 10f;
    public Slider healthSlider;
    public Text scoreText;
    private Animator animator;

    public bool pauseMenuOpened = false;

    public GameObject pauseMenu;
    public GameObject gameOverMenu;

    //Vector3 movement;
    bool dead = false;
    bool isGrounded = false;
    float distToGround;
    bool didJump = false;
    


    KeyBindManager bindManager;

    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>(); 
        granadeText = GameObject.FindGameObjectWithTag("GranadeText").GetComponent<Text>();
        healthSlider = GameObject.FindGameObjectWithTag("HealthSlider").GetComponent<Slider>();
        scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        pauseMenu.SetActive(true);
        gameOverMenu = GameObject.FindGameObjectWithTag("GameOverMenu");

        animator = GetComponent<Animator>();

        granades = maxGranades;
        rb = GetComponent<Rigidbody>();
        UpdateHealthSlider();
        distToGround = GetComponent<Collider>().bounds.extents.y;
        bindManager = GameObject.FindGameObjectWithTag("KeyBindManager").GetComponent<KeyBindManager>();

        healthSlider.maxValue = MaxHealth;
        healthSlider.value = Health;
        UpdateGranadeText();
        
        pauseMenu.GetComponent<PauseMenu>().PressedResume();
        pauseMenu.SetActive(false);
        gameOverMenu.SetActive(false);
        pauseMenuOpened = false;
    }

    private void Update()
    {
        GameManager manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        scoreText.text = "" + manager.GetScore();


        if (!pauseMenuOpened)
        {
            CheckGround();

            if (Health <= 0 && !isPlayerDead())
            {
                OnDeath();
            }

            if (!dead)
            {
                if (Input.GetKeyDown(bindManager.keys["Jump"]) && isGrounded && !didJump)
                {
                    rb.AddForce(transform.up * Mathf.Sqrt(2 * gravity * jumpHeight), ForceMode.VelocityChange);
                    didJump = true;
                    StartCoroutine(JumpCoolDown());
                }

                if(Input.GetKeyDown(bindManager.keys["Use"]))
                {
                    if(gun.gunType == Gun.GunType.Pistol)
                    {
                        gun.ChangeGun(Gun.GunType.ShotGun);
                    }
                    else
                    {
                        gun.ChangeGun(Gun.GunType.Pistol);
                    } 
                }


                UpdateRotation();

                if (Input.GetKeyDown(bindManager.keys["Shoot"]))
                {
                    gun.Shoot();
                }

                if (Input.GetKeyDown(bindManager.keys["Granade"]))
                {
                    GranadeThrow();
                }

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    PauseMenu();
                }
            }
        }
    }

    void PauseMenu()
    {
        if(pauseMenuOpened)
        {
            pauseMenu.GetComponent<PauseMenu>().PressedResume();
            pauseMenu.SetActive(false);
            pauseMenuOpened = false;
        }
        else
        {
            pauseMenu.SetActive(true);
            pauseMenuOpened = true;

            GameManager manager = FindObjectOfType<GameManager>();
            manager.PauseGame(true);
        }
    }

    void GranadeThrow()
    {
        if (granades > 0)
        {
            GameObject granade = Instantiate(granadePrefab, GranadePoint.position, GranadePoint.rotation);
            Rigidbody rb = granade.GetComponent<Rigidbody>();

            Vector3 dir = GranadePoint.forward + Vector3.up;
            rb.AddForce(dir * GranadeThrowPower, ForceMode.Impulse);
            granades--;
            UpdateGranadeText();
        }
    }

    void UpdateGranadeText()
    {
        granadeText.text = granades.ToString();
    }

    IEnumerator JumpCoolDown()
    {
        yield return new WaitForSeconds(0.1f);
        didJump = false;
    }

    void FixedUpdate()
    {
        if (!pauseMenuOpened)
        {
            rb.AddForce(-transform.up * gravity, ForceMode.Acceleration);//apply gravity

            if (!dead)
            {
                UpdateMovement();
            }
        }
    }

    void CheckGround()
    {
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    void UpdateRotation()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 playerPos = mainCamera.WorldToScreenPoint(transform.position);

        Vector3 dir = mousePos - playerPos;

        float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;

        transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
    }

    void UpdateMovement()
    {
        float H = 0, V = 0;

        if (Input.GetKey(bindManager.keys["Up"])) V = 1;
        else if (Input.GetKey(bindManager.keys["Down"])) V = -1;
        else if (Input.GetKey(bindManager.keys["Down"]) && Input.GetKeyDown(bindManager.keys["Up"])) V = 0;

        if (Input.GetKey(bindManager.keys["Left"])) H = -1;
        else if (Input.GetKey(bindManager.keys["Right"])) H = 1;
        else if (Input.GetKey(bindManager.keys["Left"]) && Input.GetKeyDown(bindManager.keys["Right"])) H = 0;


        Vector3 dirVector = new Vector3(H, 0, V).normalized * speed;

        if (dirVector.magnitude > 0)
        {
            animator.SetBool("Walking", true);
        }
        else
        {
            animator.SetBool("Walking", false);
        }

        rb.MovePosition(transform.position + dirVector * Time.deltaTime);

            /*
            //movement.Set(H, 0f, V);
            // movement = movement * speed * Time.deltaTime;

            movement = new Vector3(H, 0.0f, V).normalized;
            //movement = transform.TransformDirection(movement);

            //rb.MovePosition(transform.position + movement);
            //rb.velocity = Vector3.zero;
            movement = movement * speed;


            if (controller.isGrounded)
            {
                if (Input.GetButton("Jump"))
                {
                    movement.y = jumpForce;
                }
            }
            movement.y -= gravity * Time.deltaTime;

            controller.Move(movement * Time.deltaTime);
            */
        }


    public void TakeDamage(int Damage)
    {
        Health -= Damage;
        UpdateHealthSlider();
    }

    public void UpdateHealthSlider()
    {
        healthSlider.value = Health;
        //healthText.text = "Health:" + Health;
    }

    void OnDeath()
    {
        rb.isKinematic = true;
        Debug.Log("Dead");
        dead = true;

        gameOverMenu.SetActive(true);
        gameOverMenu.GetComponent<GameOverMenu>().addedScore = false;
        gameOverMenu.GetComponent<GameOverMenu>().UpdateTexts();
    }

    public bool isPlayerDead()
    {
        return dead;
    }

    public void AddGranades(int granades)
    {
        this.granades += granades;

        if (this.granades > maxGranades)
            this.granades = maxGranades;

        UpdateGranadeText();
    }
}
