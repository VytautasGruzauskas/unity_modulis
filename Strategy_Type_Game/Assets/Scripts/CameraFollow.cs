﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Player;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public float shakeMagnitude = 2;
    public float shakeDuration = 0.2f;

    bool isShaking = false;

    void FixedUpdate()
    {
        if(!isShaking)
        {
            Vector3 pos = Player.position + offset;
            Vector3 smoothPos = Vector3.Lerp(transform.position, pos, smoothSpeed);
            transform.position = smoothPos;
            transform.LookAt(Player);
        }
    }

    public void OnPlayerShoot()
    {
        CameraShake();
    }

    void CameraShake()
    {
        if(!isShaking)
            StartCoroutine(Shake(shakeDuration, shakeMagnitude));
    }

    IEnumerator Shake(float duration, float magnitude)
    {
        isShaking = true;
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            transform.localPosition = originalPos + Random.insideUnitSphere * shakeMagnitude;

            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPos;
        isShaking = false;
    }
}
